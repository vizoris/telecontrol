
$(function() {


// MMENU
var menu = new MmenuLight(
  document.querySelector( '#mobile-menu' ),
  'all'
);

var navigator = menu.navigation({
  // selectedClass: 'Selected',
  // slidingSubmenus: true,
  // theme: 'dark',
   title: 'Меню'
});

var drawer = menu.offcanvas({
  // position: 'left'
});

//  Open the menu.
document.querySelector( 'a[href="#mobile-menu"]' )
  .addEventListener( 'click', evnt => {
    evnt.preventDefault();
    drawer.open();
});



// Выпадающее меню
if ($(window).width() >= 992) {
   $('.dropdown, .dropdown li').hover(function() {
    $(this).children('ul').stop(true,true).fadeToggle();
   });

}else {
  $('.dropdown').click(function() {
    $(this).children('ul').stop(true,true).fadeToggle();
   })
}



 // Банер

var bannerSlider =  $('.banner-slider').slick({
  dots: false,
  arrows: false,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1,
  fade: true,
  // autoplay: true,
  // autoplaySpeed: 5000,
});
$('.banner-slider__prev').click(function(){
    $(bannerSlider).slick("slickPrev")
});
$('.banner-slider__next').click(function(){
    $(bannerSlider).slick("slickNext")
});





//  Слайдер фильтров
$('.product-filters').slick({
  dots: false,
  arrows: true,
  speed: 300,
  slidesToShow: 5,
  slidesToScroll: 1,
  variableWidth: true,
  // autoplay: true,
  // autoplaySpeed: 5000,
  responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
      }
    }
  ]
});


//  Слайдер партнерев
$('.partners-slider').slick({
  dots: false,
  arrows: false,
  speed: 300,
  slidesToShow: 6,
  slidesToScroll: 1,
  responsive: [
   {
      breakpoint: 1199,
      settings: {
        slidesToShow: 4,
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
      }
    }
  ]
});


// Плейсфолдер в инпутах
const FloatLabel = (() => {
  
  // add active class and placeholder 
  const handleFocus = (e) => {
    const target = e.target;
    target.parentNode.classList.add('active');
    
  };
  
  // remove active class and placeholder
  const handleBlur = (e) => {
    const target = e.target;
    if(!target.value) {
      target.parentNode.classList.remove('active');
    }
     
  };  
  
  // register events
  const bindEvents = (element) => {
    const floatField = element.querySelector('input,textarea');
    floatField.addEventListener('focus', handleFocus);
    floatField.addEventListener('blur', handleBlur);    
  };
  
  // get DOM elements
  const init = () => {
    const floatContainers = document.querySelectorAll('.form-item');
    
    floatContainers.forEach((element) => {
      if (element.querySelector('input,textarea').value) {
          element.classList.add('active');
      }      
      
      bindEvents(element);
    });
  };
  
  return {
    init: init
  };
})();

FloatLabel.init();


//Табы
$('.tabs-nav a').click(function() {
    var _id = $(this).attr('href');
    var _targetElement = $('.tabs').find('#' + _id);
    $('.tabs-nav a').removeClass('active');
    $(this).addClass('active');
    $('.tabs-item').removeClass('active');
    _targetElement.addClass('active');
    return false;

});







// FansyBox
 $('.fancybox').fancybox({});


 // Показать поиск

$('.header-search__btn').click(function() {
  $.fancybox.open({
  src  : '#modal-search',

  });
})



$('.faq-header').click(function(){
  if(!($(this).next().hasClass('active'))){
    $('.faq-body').slideUp().removeClass('active');
    $(this).next().slideDown().addClass('active');
  }else{
    $('.faq-body').slideUp().removeClass('active');
  };

  if(!($(this).parent('.faq-item').hasClass('active'))){
    $('.faq-item').removeClass("active");
    $(this).parent('.faq-item').addClass("active");
  }else{
    $(this).parent('.faq-item').removeClass("active");
  };
});



// Меню в сайдбаре
$('.drop >a').click(function() {
  $(this).toggleClass('active');
  $(this).next('ul').fadeToggle();
})




// сайдбар на мобильном
$('.catalog-mobile__btn').click(function() {
  $(this).toggleClass('active');
  $('.catalog-sidebar').toggleClass('active')
})







})